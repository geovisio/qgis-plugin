#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash
        # for whole tests
        python -m unittest tests.test_authentification
        # for specific test
        python -m unittest tests.test_authentification.TestAuthentication.test_api_url
"""

# standard library
import unittest
from pathlib import Path

# plugin
from panoramax.logic.authentification import AuthenticationTools

# ############################################################################
# ########## Classes #############
# ################################


class TestAuthentication(unittest.TestCase):
    """Test authentication module"""

    def test_api_url(self):
        auth = AuthenticationTools()
        url_instance = "http://example.com"

        # without description
        result = auth.build_api_generate_token_url(url_instance)
        expected_result = "http://example.com/api/auth/tokens/generate"
        self.assertEqual(expected_result, result)

        # with description
        result = auth.build_api_generate_token_url(url_instance, "fake_token")
        expected_result = (
            "http://example.com/api/auth/tokens/generate?description=fake_token"
        )
        self.assertEqual(expected_result, result)

    def test_token_loader(self):
        auth = AuthenticationTools()
        auth.load_generated_token(json_path=Path("tests/fixtures/token.json"))

    def test_jwt_checker(self):
        auth = AuthenticationTools()
        auth.load_generated_token(json_path=Path("tests/fixtures/token.json"))
        auth.check_jwt()


# ############################################################################
# ####### Stand-alone run ########
# ################################
if __name__ == "__main__":
    unittest.main()
