#! python3  # noqa: E265

"""
    Perform network request.
"""

# ############################################################################
# ########## Imports ###############
# ##################################

from pathlib import Path

# PyQGIS
from qgis.core import Qgis, QgsFileDownloader
from qgis.PyQt.QtCore import QCoreApplication, QEventLoop, QUrl
from qgis.PyQt.QtNetwork import QNetworkRequest

# project
from panoramax.toolbelt.log_handler import PlgLogger

# ############################################################################
# ########## Classes ###############
# ##################################


class NetworkRequestsManager:
    """Helper on network operations."""

    def __init__(self):
        """Initialization."""
        self.log = PlgLogger().log

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def build_request(self, url: QUrl = None) -> QNetworkRequest:
        """Build request object using plugin settings.

        :return: network request object.
        :rtype: QNetworkRequest
        """
        # if URL is not specified, let's use the default one
        if not url:
            url = self.build_url()

        # create network object
        qreq = QNetworkRequest(url=url)

        # headers
        headers = {
            b"Accept": bytes(self.plg_settings.http_content_type, "utf8"),
            b"User-Agent": bytes(self.plg_settings.http_user_agent, "utf8"),
        }
        for k, v in headers.items():
            qreq.setRawHeader(k, v)

        return qreq

    def download_file(
        self, remote_url: str, local_path: Path, method: Qgis.HttpMethod
    ) -> str:
        """Download a file from a remote web server accessible through HTTP.

        :param remote_url: remote URL
        :type remote_url: str
        :param local_path: path to the local file
        :type local_path: Path
        :param method: Method HTTP (GET or POST)
        :type Qgis.HttpMethod

        :return: output path
        :rtype: str
        """
        self.log(
            message="Downloading file from {} to {}".format(remote_url, local_path),
            log_level=4,
        )
        # download it
        loop = QEventLoop()
        file_downloader = QgsFileDownloader(
            url=QUrl(remote_url),
            outputFileName=str(local_path),
            delayStart=True,
            httpMethod=method,
        )
        file_downloader.downloadExited.connect(loop.quit)
        file_downloader.startDownload()
        loop.exec_()

        self.log(
            message="Download of {} to {} succeedeed".format(remote_url, local_path),
            log_level=3,
        )
        return local_path
