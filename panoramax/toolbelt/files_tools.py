from pathlib import Path


def file_exist(file_path: Path):
    """Check if file exist

    :param file_path: Test file path
    :type file_path: Path
    :return: Returns True if the file exists, false if it doesn't.
    :rtype: bool
    """
    return Path(file_path).exists()
