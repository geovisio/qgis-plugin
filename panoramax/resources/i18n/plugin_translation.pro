FORMS = ../../gui/dlg_authentication.ui \
        ../../gui/dlg_settings.ui

SOURCES= ../../plugin_main.py \
    ../../gui/dlg_authentication.py \
    ../../gui/dlg_settings.py \
    ../../toolbelt/log_handler.py \
    ../../toolbelt/preferences.py

TRANSLATIONS = panoramax_fr.ts
