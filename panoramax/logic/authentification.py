# standard
import json
from pathlib import Path
from typing import Optional

# PyQGIS
from qgis.PyQt.QtGui import QValidator

# Plugin
from panoramax.gui.gui_commons import QVAL_JWT
from panoramax.toolbelt import PlgLogger


class AuthenticationTools:
    """A class that handles authentication and token generation.

    This class provides methods to authenticate an instance and generate an API token.
    It also allows downloading a JSON file containing the token and extracting the claim
    URL from it.
    """

    def __init__(self):
        """Constructor method"""
        self.log = PlgLogger().log
        self._generated_token: Optional[dict] = None

    def check_jwt(self, in_jwt_token: Optional[str] = None) -> bool:
        """Check input string against JWT regex.

        :param in_jwt_token: input string to validate, defaults to None
        :type in_jwt_token: str

        :return: True if the string matches the JWT validation regex.
        :rtype: bool
        """
        # if no token passed, use the class attribute
        if in_jwt_token is None:
            in_jwt_token = self.jwt_token

        val_status, val_match, val_pos = QVAL_JWT.validate(in_jwt_token, 0)
        if val_status == QValidator.Acceptable:
            self.log(
                message="DEBUG - token is a valid JWT. 5 last chars: {}".format(
                    in_jwt_token[-5:]
                ),
                push=False,
                log_level=4,
            )
            return True

        self.log(message="Token is not a valid JWT", log_level=2)

        return False

    def build_api_generate_token_url(
        self, url_instance: str, description: str = None
    ) -> str:
        """Build API URL for token generation.

        :param url_instance: url_instance (str): Base URL instance (exemple: http://www.panoramax.ign.fr)
        :type url_instance: str
        :param description: optional token description, defaults to None
        :type description: str, optional

        :return: api url to generate token
        :rtype: str
        """
        if description is not None:
            description = f"?description={description}"
        else:
            description = ""

        api_url = f"{url_instance}/api/auth/tokens/generate{description}"
        return api_url

    def load_generated_token(self, json_path: Path) -> dict:
        """Load downloaded JSON and store it as attribute.

        :param json_path: path to the downloaded JSON field.
        :type json_path: Path

        :return: loaded JSON as dict
        :rtype: dict
        """
        try:
            self._generated_token = json.loads(json_path.read_text(encoding="UTF-8"))
        except Exception as err:
            self.log(
                message="Loading received token failed. Trace: {}".format(err),
                log_level=2,
                push=True,
            )
            raise err

    @property
    def claim_url(self) -> str:
        """Get the claim url in the json token.

        :return: link to associate the token with a user account
        :rtype: str
        """
        if (
            isinstance(self._generated_token, dict)
            and "jwt_token" in self._generated_token
        ):
            return self._generated_token.get("links", [None])[0].get("href")

    @property
    def jwt_token(self) -> str:
        """Get the JWT from the stored object..

        :return: JWT value
        :rtype: str
        """
        if (
            isinstance(self._generated_token, dict)
            and "jwt_token" in self._generated_token
        ):
            return self._generated_token.get("jwt_token")
